<?php

    require('frog.php');
    require('Ape.php');
    $sheep = new Animal("shaun");

        echo "Name : " . $sheep->name . "<br>"; // "shaun"
        echo "Legs : " . $sheep->legs. "<br>"; // 4
        echo "Cold blooded : " . $sheep->cold_blooded. "<br><br><br>"; // "no"


        $frog = new Frog("buduk");

        echo "Name : " . $frog->name . "<br>"; // "buduk"
        echo "Legs : " . $frog->legs. "<br>"; // 4
        echo "Cold blooded : " . $frog->cold_blooded. "<br>"; // "no"
        echo "Jump : " . $frog->jump . "<br><br><br>"; //"Hop Hop"


        $ape = new Ape("Kera Sakti");

        echo "Name : " . $ape->name . "<br>"; // "kera sakti"
        echo "Legs : " . $ape->legs. "<br>"; // 2
        echo "Cold blooded : " . $ape->cold_blooded. "<br>"; // "no"
        echo "Yell : " . $ape->yell . "<br>"; //"Auoo"

?>